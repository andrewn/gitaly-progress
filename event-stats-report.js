'use strict';

var collectResults = require('./collect-results');

function getEventStatsReport(since,before) {
  return collectResults(`https://gitlab.com/api/v4/projects/gitlab-org%2fgitaly/events?before=${before}`, (event) => {
    let createdAt = new Date(event.created_at);
    return createdAt < since;
  })
    .then(function(events) {
      return events.reduce((memo, event) => {
        let author = event.author && event.author.username || '';
        let actionName = event.action_name || '';
        let targetType = event.target_type || '';

        let key = `${author},${actionName},${targetType}`;

        if (memo[key]) {
          memo[key]++;
        } else {
          memo[key] = 1;
        }

        return memo;
      }, {});
    });
}

module.exports = getEventStatsReport;
