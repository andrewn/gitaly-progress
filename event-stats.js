#!/usr/bin/env node
'use strict';

const eventStatsReport = require('./event-stats-report');

eventStatsReport(new Date('2017-07-01'), '2017-10-01')
  .then(report => {
    Object.keys(report).sort().forEach(key => {
      console.log(`${key},${report[key]}`);
    });
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
  .then(() => {
    process.exit(0);
  });
