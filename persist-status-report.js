'use strict';

var collectResults = require('./collect-results');
var httpClient = require('./http-client');

function findStatusReport(statusReport) {
  return collectResults('https://gitlab.com/api/v4/projects/gitlab-org%2fgitaly/issues?labels=Status%20Report&order_by=created_at&sort=desc&scope=created-by-me&state=opened')
    .then(issues => issues.filter(issue => issue.title === statusReport.title)[0]);
}

function createNewStatusReport(statusReport) {
  return httpClient('https://gitlab.com/api/v4/projects/gitlab-org%2fgitaly/issues', 'POST', {
    title: statusReport.title,
    description: statusReport.body,
    labels: 'Status Report'
  });
}

function updateExistingStatusReport(existing, statusReport) {
  if (existing.description === statusReport.body) {
    console.error('# Report description matches');
    return;
  }

  return httpClient(`https://gitlab.com/api/v4/projects/gitlab-org%2fgitaly/issues/${existing.iid}`, 'PUT', { description: statusReport.body })
}

function persistStatusReport(statusReport) {
  return findStatusReport(statusReport)
    .then(existing => {
      if (existing) {
        return updateExistingStatusReport(existing, statusReport);
      }

      return createNewStatusReport(statusReport);
    });
}

module.exports = persistStatusReport;
