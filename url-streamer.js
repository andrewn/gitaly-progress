'use strict';

var es = require('event-stream');
var _ = require('lodash');
var parseLinks = require('parse-links');
var url = require('url');
var httpClient = require('./http-client');

var DEFAULT_PERPAGE = 100;

function getTotalPages(headers) {
  if (!headers.link) return;
  var links = parseLinks(headers.link);

  if (!links.last) return;
  var parsed = url.parse(links.last, true);
  return parseInt(parsed.query.page, 10);
}


function streamAllPages(address, stopper) {
  var urlObject = url.parse(address, true);
  delete urlObject.search;

  var query = urlObject.query;

  if (!query.per_page) query.per_page = DEFAULT_PERPAGE;
  var perPage = query.per_page;
  var initialPage = query.page || 1;

  return es.readable(function (count, callback) {
    var page = initialPage + count;
    query.page = page;
    var self = this;

    let formattedUrl = url.format(urlObject);

    httpClient(formattedUrl)
      .spread(function(results, response) {
        var lastPage = getTotalPages(response.headers) || page;

        // No results? End the stream
        if (!results || !results.length) {
          return self.emit('end');
        }

        for(let i = 0; i < results.length; i++) {
          let result = results[i];

          if (stopper && stopper(result)) {
            return self.emit('end');
          }

          self.emit('data', result);
        }

        // If we get back less than one page of results,
        // or we're on the last page, end the stream
        if (results.length < perPage || page === lastPage) {
          return self.emit('end');
        }

        callback(); // Ready to continue...
      })
      .catch(callback);
  });
}

module.exports = streamAllPages;
