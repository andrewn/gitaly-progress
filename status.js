'use strict';

var statusReport = require('./status-report');
var persistStatusReport = require('./persist-status-report');

statusReport()
  .then(report => {
    return persistStatusReport(report);
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
  .then(() => {
    process.exit(0);
  });
