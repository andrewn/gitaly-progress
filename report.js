'use strict';

var es = require('event-stream');
var urlStreamer = require('./url-streamer');
var appendSheet = require('./append-sheet');
var Promise = require('bluebird');

var googleDate = new Date().toISOString().replace('T', ' ').replace('Z', '');

class Categoriser {
  constructor(labels, defaultCategory) {
    this.labelsHash = labels.reduce((memo, l) => { memo[l] = true; return memo}, {});
    this.defaultCategory = defaultCategory;
    this.scores = labels.reduce((memo, l) => { memo[l] = 0; return memo}, {});
    this.scores[defaultCategory] = 0;
  }

  categorise(issue) {
    let labels = issue.labels;
    if (!labels || !labels.length) return this.defaultCategory;

    for (let i = 0; i < labels.length; i++) {
      let label = labels[i];
      if (this.labelsHash[label]) {
        return label;
      }
    }
    return this.defaultCategory;
  }

  score(issue) {
    if(issue.state === 'closed') return;
    let category = this.categorise(issue);

    this.scores[category]++;
  }
}

function processIssue(url, processor) {
  let count = 0;
  console.log('Processing issues')
  return new Promise((resolve, reject) => {
    urlStreamer(url)
      .pipe(es.map(function (issue, callback) {
        count++;
        processor(issue);
        callback(null, issue);
      }))
      .on('error', reject)
      .on('end', function() {
        console.log(`Processed ${count} issues`);
        resolve();
      });
  });
}

function reportBurndown() {
  var burndown = new Categoriser([
    'Migration:Ready-for-Development',
    'Migration:Client-Preparation',
    'Migration:RPC-Design',
    'Migration:Server-Impl',
    'Migration:Client-Impl',
    'Migration:Ready-for-Testing',
    'Migration:Acceptance-Testing',
    'Migration:Disabled',
    'Migration:Opt-In',
    'Migration:Opt-Out',
    'Migration:Mandatory'],
    'Other');

  return processIssue('https://gitlab.com/api/v4/projects/2009901/issues?labels=Conversation,Migration', (issue) => {
      burndown.score(issue)
    })
    .then(() => {
      let values = Object.values(burndown.scores);
      values.unshift(googleDate)

      return appendSheet('15_e940qLVqDjpMaq84RAiHATpJj3xOgZJ3khAYsKd_I', 'Tracker!A:C', values);
    });
}

function testingBacklog() {
  var acceptanceTestingBacklog = new Categoriser([
    'AT:Preparation',
    'AT:Development',
    'AT:Staging',
    'AT:Production-Initial',
    'AT:Production-Low',
    'AT:Production-Mid',
    'AT:Production-Full'],
    'Other');

  return processIssue('https://gitlab.com/api/v4/projects/2009901/issues?labels=Acceptance%20Testing', (issue) => {
      acceptanceTestingBacklog.score(issue);
    })
    .then(() => {
      let values = Object.values(acceptanceTestingBacklog.scores);
      values.unshift(googleDate)

      return appendSheet('15_e940qLVqDjpMaq84RAiHATpJj3xOgZJ3khAYsKd_I', 'AT_Tracker!A:C', values);
    });
}

return Promise.all([
    reportBurndown(),
    testingBacklog(),
  ])
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
  .then(() => {
    process.exit(0);
  });
