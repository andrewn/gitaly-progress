'use strict';

var es = require('event-stream');
var urlStreamer = require('./url-streamer');
var Promise = require('bluebird');
var httpClient = require('./http-client');
var _ = require('lodash');
var startOfWeek = require('date-fns/start_of_week');
var isToday = require('date-fns/is_today');
var subDays = require('date-fns/sub_days');
var subWeeks = require('date-fns/sub_weeks');
var addWeeks = require('date-fns/add_weeks');
var getHours = require('date-fns/get_hours');
var setHours = require('date-fns/set_hours');
var startOfDay = require('date-fns/start_of_day');
var format = require('date-fns/format');
var collectResults = require('./collect-results');

function projectFromWebUrl(webUrl) {
  return webUrl
    .replace(/^https:\/\/gitlab.com\//,'')
    .replace(/\/(issues|merge_requests)\/.*$/,'');
}

function formatIssue(issue, filterLabel) {
  let project = projectFromWebUrl(issue.web_url);

  issue.labels.sort();

  let labels = issue.labels.filter(label => label !== filterLabel)
    .map(label => `${project}~"${label}"`);

  return ` * ${project}#${issue.iid} ${issue.title} ${labels.join(' ')}`;
}

function formatIssues(issues, filterLabel) {
  return issues.map(issue => formatIssue(issue, filterLabel)).join('\n')
}

function formatMergeRequest(mergeRequest, filterLabel) {
  let project = projectFromWebUrl(mergeRequest.web_url);

  mergeRequest.labels.sort();

  let labels = mergeRequest.labels.filter(label => label !== filterLabel)
    .map(label => `${project}~"${label}"`);

  return ` * ${project}!${mergeRequest.iid} ${mergeRequest.title} by @${mergeRequest.author.username} ${labels.join(' ')}`;
}

function formatMergeRequests(mergeRequests, filterLabel) {
  return mergeRequests.map(mergeRequest => formatMergeRequest(mergeRequest, filterLabel)).join('\n')
}

function formatNoteables(noteables, filterLabel) {
  return noteables.map(noteable => {
    if (noteable.hasOwnProperty('target_branch')) {
      return formatMergeRequest(noteable, filterLabel)
    } else {
      return formatIssue(noteable, filterLabel)
    }
  }).join('\n')
}


function listMergedMergeRequests(projectPath, since) {
  let isoSince = format(startOfDay(subDays(since, 1)), 'YYYY-MM-DD');
  let projectPathEncoded = encodeURIComponent(projectPath);

  let url = `https://gitlab.com/api/v4/projects/${projectPathEncoded}/events?target_type=merge_request&action=merged&after=${isoSince}`;

  return collectResults(url)
    .then(events => {
      // Remove anything before the cutoff
      events = events.filter(event => new Date(event.created_at) >= since);

      return Promise.map(events, event => {
        return httpClient(`https://gitlab.com/api/v4/projects/${projectPathEncoded}/merge_requests/${event.target_iid}`)
          .spread(mergeRequest => mergeRequest);
      }, {
        concurrency: 5
      });
    });
}

function reportIncidents(since) {
  return collectResults('https://gitlab.com/api/v4/projects/gitlab-org%2fgitaly/issues?labels=Incident&state=all&per_page=5&order_by=created_at&sort=desc', (issue) => {
    let createdAt = new Date(issue.created_at);
    return createdAt < since;
  })
    .then(function(issues) {
      if (!issues.length) return '';

      return `
## Gitaly Incidents

${formatIssues(issues, 'Incident')}
      `;
    });
}

function reportNewGitalyGroupIssues(since) {
  return collectResults('https://gitlab.com/api/v4/groups/gitlab-org/issues?state=all&labels=Gitaly&per_page=5&order_by=created_at&sort=desc', (issue) => {
    let createdAt = new Date(issue.created_at);
    return createdAt < since;
  })
    .then(function(issues) {
      if (!issues.length) return '';

      return `
## New Gitaly gitlab-org issues

${formatIssues(issues, 'Gitaly')}`;
    });
}

function listMergedLabelledGitalyMergeRequests(since) {
  return Promise.map([
    'gitlab-org/gitlab-build-images',
    'gitlab-org/gitlab-ce',
    'gitlab-org/gitlab-ee',
    'gitlab-org/gitlab-workhorse',
    'gitlab-org/gitlab-shell',
    'gitlab-org/omnibus-gitlab'
  ],
  projectPath => listMergedMergeRequests(projectPath, since),
  { concurrency: 1 })
    .then(results => {
      let mergeRequests = _.flatten(results);

      return mergeRequests.filter(mergeRequest => mergeRequest.labels.includes('Gitaly'));
    });
}

function reportMergedMergedRequests(since) {
  return Promise.all([
    listMergedMergeRequests('gitlab-org/gitaly', since),
    listMergedLabelledGitalyMergeRequests(since)
  ])
    .then(results => {
      let mergeRequests = _.flatten(results);

      if (!mergeRequests.length) return '';

      mergeRequests = _.sortBy(mergeRequests, mergeRequest => mergeRequest.author.username);

      return `
## Merged Gitaly Merge Requests

${formatMergeRequests(mergeRequests, 'Gitaly')}`;
    });
}

function getNoteUrl(projectPathEncoded, note) {
  if (!note) return;

  if (note.noteable_type === 'MergeRequest') {
    return `https://gitlab.com/api/v4/projects/${projectPathEncoded}/merge_requests/${note.noteable_iid}`;
  }

  if (note.noteable_type === 'Issue') {
    return `https://gitlab.com/api/v4/projects/${projectPathEncoded}/issues/${note.noteable_iid}`;
  }
}

function mostDiscussed(projectPath, since) {
  let isoSince = format(startOfDay(since), 'YYYY-MM-DD');
  let projectPathEncoded = encodeURIComponent(projectPath);

  let url = `https://gitlab.com/api/v4/projects/${projectPathEncoded}/events?action=commented&after=${isoSince}`

  return collectResults(url)
    .then(events => {
      // Remove anything before the cutoff
      events = events.filter(event => new Date(event.created_at) >= since);

      let folded = events.reduce((memo, event) => {
        let url = getNoteUrl(projectPathEncoded, event.note);

        if (url) {
          if (memo[url]) {
            memo[url]++;
          } else {
            memo[url] = 1;
          }
        }

        return memo;
      }, {});

      let discussionCounts = Object.values(folded);

      var topFilter = _.max(discussionCounts);

      // Sometimes we add some more items
      if (topFilter > 10) {
        let second = _.max(discussionCounts.filter(n => n < topFilter));
        if (second > 2) {
          topFilter = second;
        }
      }

      let mostDiscussed = _.pickBy(folded, count => count >= topFilter);

      return Promise.map(Object.keys(mostDiscussed), url => {
        return httpClient(url)
          .spread(mergeRequest => mergeRequest);
      }, { concurrency: 1 });

    })
    .then(noteables => {
      if (!noteables.length) return '';

      return `
## Most Discussed Items

${formatNoteables(noteables)}`;
    });
}

function startTime() {
  var now = new Date();

  var wednesday = startOfWeek(now, { weekStartsOn: 3 });

  if(isToday(wednesday)) {
    if (getHours(now) < 16) {
      wednesday = subWeeks(wednesday, 1);
    }
  }

  // Switchover time is always 16h00UTC on a Wednesday
  return setHours(wednesday, 16);
}

function getStatusReport() {
  let since = startTime();
  let until = addWeeks(since, 1);

  return Promise.all([
    mostDiscussed('gitlab-org/gitaly', since),
    reportIncidents(since),
    reportNewGitalyGroupIssues(since),
    reportMergedMergedRequests(since),
  ])
    .spread((mostDiscussedItems, incidents, newGitalyGroupIssues, mergedMergeRequests) => {
      return  {
        title: `Gitaly Status Report: ${format(since, 'YYYY-MM-DD')} - ${format(until, 'YYYY-MM-DD')}`,
        body: `
# Gitaly Burndown

Downwards indicates progress. Upwards indicates new tasks.

|**Migrations**|**Testing**|
|------------|---------|
| [![](https://docs.google.com/spreadsheets/d/e/2PACX-1vRu_c_QkN_gjYToSA9q49RV4431rGBW7x3HIwVI7mhF1FGllBvP1i-xdsqnholz2AXBYvQHUGNOHqJo/pubchart?oid=927753702&format=image)](https://docs.google.com/spreadsheets/d/15_e940qLVqDjpMaq84RAiHATpJj3xOgZJ3khAYsKd_I/edit#gid=1076898327) | [![](https://docs.google.com/spreadsheets/d/e/2PACX-1vRu_c_QkN_gjYToSA9q49RV4431rGBW7x3HIwVI7mhF1FGllBvP1i-xdsqnholz2AXBYvQHUGNOHqJo/pubchart?oid=1349554074&format=image)](https://docs.google.com/spreadsheets/d/15_e940qLVqDjpMaq84RAiHATpJj3xOgZJ3khAYsKd_I/edit#gid=595696276) |

${mostDiscussedItems}

${incidents}

${mergedMergeRequests}

${newGitalyGroupIssues}

-------------------------

This status report was [autogenerated](https://gitlab.com/andrewn/gitaly-progress/).
`
      };
    });
}

module.exports = getStatusReport;
